db = connect('weather');

const dbUser = db.getUser('centaurtek');

if (!dbUser) {
    db.createUser({
        user: 'centaurtek',
        pwd: '1234556',
        roles: ['readWrite']
    });
}

function createMeasurement (data) {
    data._id = new ObjectId();
    data.createdAt = Date.now();
    data.date = Date.now();
    data.locationId = new ObjectId();

    return data;
}

function createLocation (data) {
    data._id = new ObjectId();
    data.createdAt = Date.now();

    return data;
}

const locationVolos = createLocation({
    name: 'Volos',
    lat: 39.361890,
    long: 22.942071,
    intervalPeriod: 5
});

const locationLarisa = createLocation({
    name: 'Larisa',
    lat: 39.638395,
    long: 22.418847,
    intervalPeriod: 3
});

db.Locations.insert(locationVolos);
db.Locations.insert(locationLarisa);