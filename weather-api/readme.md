# Weather Application

## Installation instructions

### Server
Please follow the instructions below:

Open terminal and go to the weather-api folder. Now run the following commands:

1. npm install
2. mongo scripts/init.js
3. npm run build
4. npm run start

Server starts at port 8080

### Client

Open another one terminal and go to the weather folder. Now run the following commands:

1. npm install
2. npm run gulp
3. npm run build
4. npm run server

Ok server starts at port 3000

I hope you like it!!!
