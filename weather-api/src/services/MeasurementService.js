import mongoose from 'mongoose';
import q from 'q';

export default class MeasurementService {
    constructor (datastore, locationService, measurementModel, websocket) {
        this._datastore = datastore;
        this._locationService = locationService;
        this._measurementModel = measurementModel;
        this._websocket = websocket;
    }


    getMeasurementsByLocation (locationId) {
        const self = this,
            deferred = q.defer();

        locationId = locationId.toString();

        self._datastore.model('Measurement')
            .where('locationId').eq(locationId)
            .limit(100)
            .exec((error, result) => {
                if (error) {
                    console.log(error);
                    deferred.reject(error);
                }

                deferred.resolve(result);
            });

        return deferred.promise;
    }

    createMeasurement (measurement) {
        const model = this._measurementModel.createModel(measurement);

        model.save((error) => {
            if (error) {
                console.log(error);
            }
        });

        // Broadcast an event to connected clients
        var WebsocketsocketClients = this._websocket.getWss().clients;
        let WebsocketsocketClientsLength = this._websocket.getWss().clients.length;
        for(let i = 0; i < WebsocketsocketClientsLength; i++) {
            WebsocketsocketClients[i].send('MEASUREMENT_CREATED');
        }
    }

    removeMeasurementsFromLocation (locationId) {
        const self = this,
            deferred = q.defer();

        self._datastore.model('Measurement').find({locationId: locationId}).remove().exec((error, data) => {
            if (error) {
                console.log(error);
                deferred.reject(error);
            }

            deferred.resolve(data);
        });

        return deferred.promise;
    }
}
