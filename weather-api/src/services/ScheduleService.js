import Forecast from '../utilities/Forecast';
import DatetimeUtility from '../utilities/DatetimeUtility';

export default class ScheduleService {

    /**
     * Constructor
     * @param measurementService
     * @param locationService
     */
    constructor (measurementService, locationService) {
        this._measurementService = measurementService;
        this._locationService = locationService;
    }

    /**
     * Run Scheduler
     */
    run () {
        const self = this;

        self._locationService.getLocations().then((result) => {
            const resultLength = result.length;
            for (let i = 0; i < resultLength; i++) {
                self.start(result[i]);
            }
        });
    }

    /**
     * Set Interval timer
     * @param location
     */
    start (location) {
        const self = this;
        const interval = setInterval(() => {
            // Log the tick
            this._locationService.getLocationById(location._id).then((location) => {
                // Log
                console.log('Get temperatures - ' + location.name);

                // Create a new forecast object.
                let forecast = new Forecast(location.lat, location.long);

                // Make request to dark sky api.
                forecast.makeRequest().then((result) => {
                    let currently = result.currently;
                    let measurement = {
                        temperature: currently.temperature,
                        humidity: currently.humidity,
                        date: Date.now(),
                        locationId: location._id
                    };

                    // Store measurement to mongo
                    self._measurementService.createMeasurement(measurement);
                });
            }, (error) => {
                clearInterval(interval);
            });
        }, DatetimeUtility().minutesToMiliseconds(location.intervalPeriod));
    }
}
