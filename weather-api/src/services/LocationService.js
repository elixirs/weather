import mongoose from 'mongoose';
import Forecast from '../utilities/Forecast';
import q from 'q';

export default class LocationService {

    /**
     * Constructor
     * @param datastore
     * @param locationModel
     */
    constructor (datastore, locationModel) {
        this._datastore = datastore;
        this._locationModel = locationModel;
    }

    /**
     * Setter measurementService
     * @param service
     */
    set measurementService (service) {
        this._measurementService = service;
    }

    set scheduleService (service) {
        this._scheduleService = service;
    }

    /**
     * Get all locations
     * @returns {*}
     */
    getLocations () {
        const self = this,
            deferred = q.defer();

        self._datastore.model('Location')
            .find({})
            .exec((error, result) => {
                if (error) {
                    console.log(error);
                    deferred.reject(error);
                }

                if (!result) {
                    deferred.reject('RESOURCE_NOT_FOUND');
                }

                deferred.resolve(result);
            });

        return deferred.promise;
    }

    /**
     * Get location by id
     * @param locationId
     * @returns {*}
     */
    getLocationById (locationId) {
        const self = this,
            deferred = q.defer();

        self._datastore.model('Location')
            .find({_id: locationId})
            .exec((error, result) => {
                if (error) {
                    console.log(error);
                    deferred.reject(error);
                }

                if (!result) {
                    deferred.reject('RESOURCE_NOT_FOUND');
                }

                if (result.length == 0) {
                    deferred.reject('RESOURCE_NOT_FOUND');
                }

                deferred.resolve(result[0]);
            });

        return deferred.promise;
    }

    /**
     * Create new location
     * @param location
     */
    createLocation (location) {
        const self = this,
            model = this._locationModel.createModel(location),
            deferred = q.defer();

        model.save((error) => {
            if (error) {
                console.log(error);
                deferred.reject(error)
            }

            let forecast = new Forecast(location.lat, location.long);
            // Make request to dark sky api.
            forecast.makeRequest().then((result) => {
                const currently = result.currently,
                    measurement = {
                    temperature: currently.temperature,
                    humidity: currently.humidity,
                    date: Date.now(),
                    locationId: model._id
                };

                // Store measurement to mongo
                self._measurementService.createMeasurement(measurement);

                // Start schedule for location who created
                self._scheduleService.start(model);

                // resolve
                deferred.resolve(model);

            }, (error) => {
                deferred.reject(error);
            });
        });

        return deferred.promise;
    }
}
