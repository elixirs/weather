import BaseModel from './BaseModel';
import mongoose from 'mongoose';

export default class Measurement extends BaseModel {
    constructor () {
        super();
    }

    /**
     * Setup Model
     */
    setup () {
        // Initialize Model.
        let Schema = mongoose.Schema;
        let measurementSchema = new Schema({
            createdAt: {
                type: Date,
                default: Date.now
            },
            temperature: Number,
            humidity: Number,
            date: Date,
            locationId: String
        }, {
            collection: 'Measurements'
        });

        // Set model.
        this._model = mongoose.model('Measurement', measurementSchema);
    }
}
