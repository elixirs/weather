export default class BaseModel {
    constructor () {
        this.setup();
    }

    /**
     * Create Model
     * @param data
     * @returns {Measurement._model}
     */
    createModel (data) {
        return new this._model(data);
    }
}
