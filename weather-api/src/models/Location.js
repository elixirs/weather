import BaseModel from './BaseModel';
import mongoose from 'mongoose';

export default class Location extends BaseModel {
    constructor () {
        super();
    }

    /**
     * Setup Model
     */
    setup () {
        // Initialize Model.
        let Schema = mongoose.Schema;
        let locationSchema = new Schema({
            createdAt: {
                type: Date,
                default: Date.now
            },
            name: String,
            lat: Number,
            long: Number,
            intervalPeriod: Number
        }, {
            collection: 'Locations'
        });

        // Set model.
        this._model = mongoose.model('Location', locationSchema);
    }
}