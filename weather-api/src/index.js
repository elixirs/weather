"use strict";

// Define modules.
import config from './config.json';
import express from 'express';
import mongoose from 'mongoose';
import bodyparser from 'body-parser';
import enableCORS from './utilities/EnableCORS';
import expressWs from 'express-ws';

// Declare App
let app = express();

// Configure web socket
const websocket = expressWs(app);
// Websocket Controller
app.ws('/socket', (ws, req) => {});

// Define Controllers and Services.
import Datastore from './utilities/Datastore';
import { MeasurementController, LocationController } from './controllers/Controllers'
import { Measurement, Location } from './models/Models';
import { LocationService, MeasurementService, ScheduleService } from './services/Services';

// Initialize MongoDb Connection.
const datastoreObject = new Datastore(config.database);
const datastore = datastoreObject.datastore;

// Initialize Models.
const measurementModel = new Measurement();
const locationModel = new Location();

// Initialize services.
const locationService = new LocationService(datastore, locationModel);
const measurementService = new MeasurementService(datastore, locationService, measurementModel, websocket);
const scheduleService = new ScheduleService(measurementService, locationService);
// Set measurement Service into location service.
locationService.measurementService = measurementService;
locationService.scheduleService = scheduleService;

// Initialize controllers.
const measurementController = new MeasurementController(locationService, measurementService);
const locationController = new LocationController(locationService);

// Enable Json Body
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
   extended: true
}));

// Enable CORS
app.use(enableCORS());

// Setup Api with routes
app.use(measurementController.router);
app.use(locationController.router);

// Run all Schedules
scheduleService.run();

app.listen(config.server.port, () => {
   console.log('Server Start!');
});