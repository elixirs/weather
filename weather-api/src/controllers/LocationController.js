import BaseController from './BaseController';
import q from 'q';

export default class LocationController extends BaseController {

    /**
     * Constructor
     * @param locationService
     * @param measurementService
     */
    constructor (locationService, measurementService) {
        super();
        this._locationService = locationService;
        this._measurementService = measurementService;
    }

    /**
     * Set endpoints
     */
    setup () {
        this.router.get('/api/locations', this.getLocations.bind(this));
        this.router.post('/api/locations', this.createLocation.bind(this));
    }

    /**
     * Get locations endpoint
     * @param request
     * @param response
     * @param next
     */
    getLocations (request, response, next) {
        const self = this;

        // Get locations
        self._locationService.getLocations().then((locations) => {
            // Set status
            response.status(200);
            // Set Headers
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Cachce-Control', 'no-cache');
            response.send(locations);
            next();

        }, (error) => {
            // Log error
            console.log(error);
            // Set status
            response.status(404);
            // Set Headers
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Cachce-Control', 'no-cache');
            response.send({});
            next();
        });
    }

    /**
     * Create location endpoint
     * @param request
     * @param response
     * @param next
     */
    createLocation (request, response, next) {
        const self = this;

        // New location object
        let location = {
            name: request.body.name,
            lat: request.body.lat,
            long: request.body.long,
            intervalPeriod: request.body.intervalPeriod
        };

        self._locationService.createLocation(location).then(() => {
            self._locationService.getLocations().then((locations) => {
                // Set status
                response.status(200);
                // Set Headers
                response.setHeader('Content-Type', 'application/json');
                response.setHeader('Cachce-Control', 'no-cache');
                response.send(locations);
                next();
            }, (error) => {
                // Log error
                console.log(error);
                // Set status
                response.status(404);
                // Set Headers
                response.setHeader('Content-Type', 'application/json');
                response.setHeader('Cachce-Control', 'no-cache');
                response.send({});
                next();
            });
        }, (error) => {
            // Log error
            console.log(error);
            // Set status
            response.status(404);
            // Set Headers
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Cachce-Control', 'no-cache');
            response.send({});
            next();
        });
    }
}