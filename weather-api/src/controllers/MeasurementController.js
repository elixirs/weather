import BaseController from './BaseController';
import Forecast from '../utilities/Forecast';
import q from 'q';

export default class MeasurementController extends BaseController {

    /**
     * Constructor
     * @param locationService
     * @param measurementService
     */
    constructor(locationService, measurementService) {
        super();
        this._locationService = locationService;
        this._measurementService = measurementService;
    }

    /**
     * Set endpoints
     */
    setup() {
        this.router.get('/api/measurements', this.getMeasurements.bind(this));
        this.router.get('/api/measurements/:id', this.getMeasurementsByLocation.bind(this));
        this.router.post('/api/measurements', this.deleteMeasurementsFromLocation.bind(this));
    }

    /**
     * Get measuremnts with locations
     * @param request
     * @param response
     * @param next
     */
    getMeasurements(request, response, next) {
        const self = this;

        self._locationService.getLocations().then((locations) => {
            const locationsLength = locations.length;
            let promises = [];

            for (let i = 0; i < locationsLength; i++) {
                promises[i] = self._measurementService.getMeasurementsByLocation(locations[i]._id).then((result) => {
                    return result;
                });
            }

            q.all(promises).then((result) => {
                let output = {};
                for (let i = 0; i < locationsLength; i++) {
                    output[locations[i].name] = result[i];
                }
                // Set status
                response.status(200);
                // Set Headers
                response.setHeader('Content-Type', 'application/json');
                response.setHeader('Cachce-Control', 'no-cache');
                response.send(output);
                next();
            }, (error) => {
                // Log error
                console.log(error);
                // Set status
                response.status(203);
                // Set Headers
                response.setHeader('Content-Type', 'application/json');
                response.setHeader('Cachce-Control', 'no-cache');
                response.send({});
                next();
            });
        }, (error) => {
            // Log error
            console.log(error);
            // Set status
            response.status(404);
            // Set Headers
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Cachce-Control', 'no-cache');
            response.send({});
            next();
        });
    }

    /**
     * Get measurements by location
     * @param request
     * @param response
     * @param next
     */
    getMeasurementsByLocation (request, response, next) {
        const self = this, output = {};

        self._measurementService.getMeasurementsByLocation(request.params.id).then((measurements) => {
            self._locationService.getLocationById(request.params.id).then((location) => {
                output[location.name] = measurements;

                // Set status
                response.status(200);
                // Set Headers
                response.setHeader('Content-Type', 'application/json');
                response.setHeader('Cachce-Control', 'no-cache');
                response.send(output);
                next();

            }, (error) => {
                // Log error
                console.log(error);
                // Set status
                response.status(404);
                // Set Headers
                response.setHeader('Content-Type', 'application/json');
                response.setHeader('Cachce-Control', 'no-cache');
                response.send({});
                next();
            });
        });
    }

    deleteMeasurementsFromLocation (request, response, next) {
        const self = this;

        self._measurementService.removeMeasurementsFromLocation(request.body.locationId).then((data) => {
            // Set status
            response.status(200);
            // Set Headers
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Cachce-Control', 'no-cache');
            response.send();
            next();
        }, (error) => {
            // Log error
            console.log(error);
            // Set status
            response.status(404);
            // Set Headers
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Cachce-Control', 'no-cache');
            response.send({});
            next();
        });
    }
}