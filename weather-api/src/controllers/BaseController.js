import express from 'express';

export default class BaseController {

    constructor () {
        this._router = express.Router();
        this.setup();
    }

    get router () {
        return this._router;
    }
}
