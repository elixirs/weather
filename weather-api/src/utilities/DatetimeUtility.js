export default function () {
    return {
        minutesToMiliseconds: (minutes) => {
            return minutes * 60 * 1000;
        }
    };
}
