import mongoose from "mongoose";

export default class Datastore {
    constructor (config) {
        this._datastore = mongoose.connect(
            'mongodb://' + config.user + ':' + config.password + '@' + config.host + ':' + config.port + '/' + config.name
        );
    }

    get datastore () {
        return this._datastore;
    }
}
