import config from '../config.json';
import request from 'request';
import q from 'q';

export default class Forecast {
    constructor (lat, long) {
        const forecastConfig = config.forecast;

        // Initialize object.
        this._apiKey = forecastConfig.apiKey;
        this._requestUrl = forecastConfig.requestUrl;
        this._units = forecastConfig.units;
        this._lang = forecastConfig.lang;
        this._lat = lat;
        this._long = long;
    }

    getRequestUrl () {
        return this._requestUrl + '/' + this._apiKey + '/' + this._lat + ',' + this._long + '/?units=' + this._units + '&lang=' + this._lang;
    }

    makeRequest () {
        const requestUrl = this.getRequestUrl(),
            deferred = q.defer();

        request.get(requestUrl, (error, response, body) => {
            if (!error && response.statusCode == 200) {
                try {
                    deferred.resolve(JSON.parse(body));
                } catch (error) {
                    deferred.reject(error);
                }
            } else {
                deferred.reject(error);
            }
        });

        return deferred.promise;
    }
}
