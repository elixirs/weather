'use strict';

module.exports = {
    source: 'src',
    dist: 'dist',
    tasks: {
        html: {
            input: '*.html'
        },
        styles: {
            folder: 'styles',
            input: '*.css'
        },
        fonts: {
            folder: 'fonts',
            input: '*.*'
        },
        scripts: {
            folder: 'scripts/vendor',
            input: '**/*.js'
        }

    }
};
