import React from 'react';

export default class Modal extends React.Component {

    render () {
        return (
            <div id={this.props.id} className="uk-modal" >
                <div className="uk-modal-dialog">
                    <a className="uk-modal-close uk-close"></a>
                    <div className="uk-modal-header">
                        <h3 className="uk-h3 uk-text-center">Add new location</h3>
                    </div>
                    <div>
                        {this.props.children}
                    </div>
                    <div className="uk-modal-footer">
                        <div className="uk-float-right" >
                            <button className="uk-button uk-button-success uk-margin-right" onClick={this.props.onclickok} >Ok</button>
                            <button className="uk-button uk-button-danger" onClick={this.closeModal.bind(this)} >Cancel</button>
                        </div>
                        <div className="uk-clearfix" ></div>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount () {
        this.modal = UIkit.modal('#' + this.props.id);
    }

    closeModal () {
        this.modal.hide();
    }
}
