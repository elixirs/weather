import React from 'react';
import moment from 'moment';

export default class JqPlot extends React.Component {

    /**
     * Constructor
     */
    constructor () {
        super();
        window.ws.onmessage = this.webSocketEvent.bind(this);
    }

    /**
     * Render Page
     * @returns {XML}
     */
    render () {
        return (
            <div>
                <div className="uk-width-1-1 uk-text-center uk-h3" id="no-data" >Please wait. Temperatures not found at this moment.</div>
                <div id="plot" className="uk-width-1-1 uk-text-center" height="400" ></div>
                <button className="uk-button uk-button-primary uk-button-large uk-float-right" onClick={this.resetZoom.bind(this)}>Reset zoom</button>
            </div>
        );
    }

    componentWillUpdate (nextProps, nextState) {
        $.get(this.props.datasource, function (result) {
            this.renderPlot(result);
        }.bind(this));
    }
    /**
     * React event after render
     */
    componentDidMount () {
        $.get(this.props.datasource, function (result) {
            this.renderPlot(result);
        }.bind(this));
    }

    /**
     * Websocket event
     * @param message
     */
    webSocketEvent (message) {
        if (message.data == 'MEASUREMENT_CREATED') {
            $.get(this.props.datasource, function (result) {
                this.renderPlot(result);
            }.bind(this));
        }
    }

    /**
     * Render plot to the page
     * @param data
     */
    renderPlot (data) {
        this.data = this.convertDataToPlotSchema(data);
        if (this.hasData(this.data.lines)) {

            $('#no-data').css({display: 'none'});

            $.jqplot.config.enablePlugins = true;

            const options = {
                title: 'Plot (Temperature/Time)',
                series: this.data.labels,
                legend: {
                    show: true,
                    placement: 'inside'
                },
                seriesDefaults: {
                    showMarker: true,
                    pointLabels: {show: false},
                    rendererOptions: {
                        padding: 10
                    }
                },
                axes: {
                    xaxis: {
                        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                        renderer: $.jqplot.DateAxisRenderer,
                        label: 'Time',
                        tickOptions: {
                            fontSize: '14px',
                            fontFamily: 'Calibri',
                            angle: -90
                        }
                    },
                    yaxis: {
                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                        pad: 10,
                        label: 'Temperatures (°C)',
                        tickOptions: {
                            fontSize: '14px',
                            fontFamily: 'Calibri',
                            angle: 0,
                            showMark: false,
                            formatString: "%#.2f °C"
                        }
                    }
                },
                grid: {
                    background: '#fff'
                },
                cursor: {
                    zoom: true,
                    looseZoom: true,
                    constrainOutsideZoom: false
                }
            };

            // Destroy if plot exists and reconstruct
            if (this.plot) {
                this.plot.destroy();
            }
            this.plot = $.jqplot('plot', this.data.lines, options);
            this.resize();
        } else {
            if (this.plot) {
                this.plot.destroy();
            }
            $('#no-data').css({display: 'block'});
        }

    }

    /**
     * Convert data from server to jqplot schema
     * @param data
     * @returns {{labels: Array, lines: Array}}
     */
    convertDataToPlotSchema (data) {
        let plotData = {
            labels: [],
            lines: []
        };

        for (let i in data) {
            if (i != 'undefined') {
                let weatherData = data[i],
                    lineData = [],
                    weatherDataLength = weatherData.length;

                if (weatherDataLength > 0) {
                    plotData.labels.push({label: i});
                    for (let y = 0; y < weatherDataLength; y++) {
                        lineData.push([moment(weatherData[y].date).format('l LTS'), weatherData[y].temperature]);
                    }
                    plotData.lines.push(lineData);
                }
            }
        }
        return plotData;
    }

    /**
     * Check if data are empty
     * @param lines
     * @returns {boolean}
     */
    hasData (lines) {
        let linesLength = lines.length;
        for(let i = 0; i < linesLength; i++) {
            if (lines[i].length > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Reset zoom
     */
    resetZoom () {
        this.plot.resetZoom();
    }

    /**
     * Make jqplot mobile friendly
     */
    resize () {
        $(window).on('resize', function () {
            this.plot.replot();
        }.bind(this));
    }
}
