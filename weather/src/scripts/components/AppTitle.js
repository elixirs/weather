import React from 'react';
import NavMenu from './NavMenu';

export default class AppTitle extends React.Component {
    render() {
        this.setPageTitle(this.props.title);
        return (
            <div className="tm-header uk-width-1-1">
                <NavMenu />
                <h1 className="uk-h1 uk-text-center">{this.props.title}</h1>
            </div>
        );
    }

    setPageTitle(title) {
        $('head title').text(title);
    }
}

