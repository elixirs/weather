import React from 'react';
import AppTitle from '../AppTitle';

export default class NotFoundComponent extends React.Component {

    render () {
        return (
            <div className="uk-width-1-1">
                <AppTitle title="404 - Page Not Found" ></AppTitle>
            </div>
        );
    }

}