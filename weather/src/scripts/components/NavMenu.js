import React from 'react';
import { Link } from 'react-router';

export default class NavMenu extends React.Component {
    render () {
        return (
            <div>
                <div className="uk-flex uk-float-left">
                    <a href="#menu" data-uk-offcanvas><i className="uk-icon-navicon"></i></a>
                </div>
                <div id="menu" className="uk-offcanvas">
                    <div className="uk-offcanvas-bar">
                        <ul className="uk-nav">
                            <li><Link to="/" >Home</Link></li>
                            <li><Link to="/locations" >Locations</Link></li>
                        </ul>
                    </div>
                </div>
            </div>

        );
    }
}
