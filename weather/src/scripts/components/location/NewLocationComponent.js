import React from 'react';
import AppTitle from '../AppTitle';

export default class NewLocationComponent extends React.Component {

    render() {
        return (
            <div class="uk-width-1-1">
                <AppTitle title="Weather App - Add New Location" ></AppTitle>
                <div className="uk-container uk-width-1-1"></div>
            </div>
        );
    }

}
