import React from 'react';
import { Link } from 'react-router'
import AppTitle from '../AppTitle';
import Modal from '../Modal';

export default class LocationsComponent extends React.Component {

    constructor () {
        super();

        this.state = {
            data: []
        };
    }

    componentWillMount () {
        $.get('http://localhost:8080/api/locations', function (result) {
            this.setState({ data: result });
        }.bind(this));
    }

    componentWillUpdate (nextProps, nextState) {
        this.render();
    }

    render () {
        return (
            <div className="uk-width-1-1">
                <AppTitle title="Weather App - Locations" ></AppTitle>
                <div className="uk-container uk-container-center uk-margin-top uk-margin-bottom uk-width-1-1">
                    {this.state.data.map(this.renderLink)}
                    <a href="" className="uk-float-right uk-button uk-button-primary uk-button-large uk-margin-top" data-uk-modal="{target: '#add-new-location-modal'}" >Add new location</a>
                    <Modal id="add-new-location-modal" onclickok={this.formSubmit.bind(this)}>
                        <form className="uk-form" id="add-new-location-form">
                            <fieldset data-uk-margin>
                                <div className="uk-grid" >
                                    <div className="uk-width-1-1 uk-width-medium-1-2">
                                        <input type="text" className="uk-width-1-1" placeholder="City,Country" id="city-country" />
                                    </div>
                                    <div className="uk-width-1-1 uk-width-medium-1-2">
                                        <div className="uk-button uk-button-primary uk-width-1-1" onClick={this.getCoordinates.bind(this)}>Get coordinates</div>
                                    </div>
                                </div>
                                <div className="uk-form-row">
                                    <input type="text" className="uk-width-1-1" placeholder="Location name" name="name" />
                                </div>
                                <div className="uk-form-row">
                                    <div className="uk-grid" >
                                        <div className="uk-width-1-1 uk-width-medium-1-2">
                                            <input type="text" className="uk-width-1-1" placeholder="Lat" name="lat" />
                                        </div>
                                        <div className="uk-width-1-1 uk-width-medium-1-2">
                                            <input type="text" className="uk-width-1-1" placeholder="Long" name="long" />
                                        </div>
                                    </div>
                                </div>
                                <div className="uk-form-row">
                                    <input type="number" className="uk-width-1-1" name="intervalPeriod" placeholder="Interval Period (Minutes)" />
                                </div>
                            </fieldset>
                        </form>
                    </Modal>
                </div>
            </div>
        );
    }

    renderLink (location) {
        const url = 'location/' + location._id + '/' + location.name;
        return (
            <Link to={url} className="uk-button uk-button-large uk-width-1-1 uk-margin-top" key={location._id} >
                {location.name}
            </Link>
        );
    }

    formSubmit () {
        this.form = $('#add-new-location-form');

        if ( this.formValidation() ) {
            $.ajax({
                url: 'http://localhost:8080/api/locations',
                type: 'POST',
                data: this.getFormData(),
                dataType: 'json',
                beforeSend: function() {
                    this.blockUI = UIkit.modal.blockUI("Please wait...");
                }.bind(this),
                success: function (result) {
                    this.clearForm();
                    this.blockUI.hide();
                    UIkit.modal('#add-new-location-modal').hide();
                    this.setState({ data: result });
                }.bind(this)
            });
        }
    }

    formValidation () {
        let nameValid = (this.form.find('input[name="name"]').val() != ''),
            latValid = (this.form.find('input[name="lat"]').val() != ''),
            longValid = (this.form.find('input[name="long"]').val() != ''),
            intervalPeriodValid = (this.form.find('input[name="intervalPeriod"]').val() != '');

        return nameValid && latValid && longValid && intervalPeriodValid;
    }


    getFormData () {
        const unindexed_array = this.form.serializeArray();
        let indexed_array = {};

        $.map(unindexed_array, (name, index) => {
            indexed_array[name['name']] = name['value'];
        });

        return indexed_array;
    }

    clearForm () {
        this.form.find('input[name="name"]').val("");
        this.form.find('input[name="lat"]').val("");
        this.form.find('input[name="long"]').val("");
        this.form.find('input[name="intervalPeriod"]').val("");
    }

    getCoordinates () {
        this.form = $('#add-new-location-form');

        const cityCountry = $('#city-country').val();

        let lat = this.form.find('input[name="lat"]'),
            long = this.form.find('input[name="long"]');

        $.get('http://maps.google.com/maps/api/geocode/json?address=' + cityCountry, (result) => {
            const results = result.results;

            if (results.length > 0) {
                let coordinates = results[0].geometry.location;
                lat.val(coordinates.lat);
                long.val(coordinates.lng);
            }
        });
    }
}

