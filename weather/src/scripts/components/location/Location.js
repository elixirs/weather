import LocationsComponent from './LocationsComponent';
import LocationComponent from './LocationComponent';
import NewLocationComponent from './NewLocationComponent';

export { LocationsComponent, LocationComponent, NewLocationComponent };