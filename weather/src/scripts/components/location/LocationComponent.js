import React from 'react';
import AppTitle from '../AppTitle';
import JqPlot from '../JqPlot';

export default class LocationComponent extends React.Component {

    constructor () {
        super();

        this.state = {
            changeFlag : true,
            data: []
        }
    }

    componentWillUpdate (nextProps, nextState) {
        this.render();
    }

    render () {
        const datasource='http://localhost:8080/api/measurements/' + this.props.params.locationId,
            title = 'Weather App - ' + this.props.params.locationName;
        return (
            <div className="uk-width-1-1">
                <AppTitle title={title} ></AppTitle>
                <div className="uk-container uk-container-center uk-margin-top">
                    <JqPlot data-state={this.state.changeFlag} datasource={datasource}></JqPlot>
                </div>
                <div className="uk-container uk-container-center uk-margin-top">
                    <button id="clear-data" className="uk-button uk-button-danger uk-button-large uk-float-right"
                        data-id={this.props.params.locationId}
                        onClick={this.clearData.bind(this)}>Clear Data</button>
                </div>
            </div>
        );
    }

    clearData () {
        $.ajax({
            url: 'http://localhost:8080/api/measurements',
            type: 'POST',
            data: {
                locationId: $('#clear-data').data('id')
            },
            dataType: 'json',
            beforeSend: function () {
                this.blockUI = UIkit.modal.blockUI("Please wait...");
            }.bind(this),
            complete: function (){
                this.blockUI.hide();
                const changeFlag = (this.state.changeFlag == true) ? false : true;
                this.setState({
                    changeFlag: changeFlag
                });
            }.bind(this)
        });
    }
}