import React from 'react';
import AppTitle from './AppTitle';
import JqPlot from './JqPlot';

/**
 * The main App component
 */
export default class AppComponent extends React.Component {
    render () {
        return (
            <div className="uk-width-1-1">
                <AppTitle title="Weather App" ></AppTitle>
                <div className="uk-container uk-container-center uk-margin-top">
                    <JqPlot datasource="http://localhost:8080/api/measurements"></JqPlot>
                </div>
            </div>
        );
    }
}