import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, hashHistory } from 'react-router';
import AppComponent from './components/AppComponent';
import { LocationsComponent, LocationComponent, NewLocationComponent } from './components/location/Location';
import { NotFoundComponent } from './components/error/Error';

// Web Socket Configuration
// Open new web socket connection
window.ws = new WebSocket('ws://localhost:8080/socket');

window.ws.onopen = () => {
    // Web Socket is connected, send data using send()
    console.warn('Web socket is connected');
};

render (
    (
        <Router history={hashHistory}>
            <Route path="/" component={AppComponent}></Route>
            <Route path="/locations" component={LocationsComponent} ></Route>
            <Route path="/location/:locationId/:locationName" component={LocationComponent} ></Route>
            <Route path="/location/new" component={NewLocationComponent} ></Route>
            <Route path="*" component={NotFoundComponent} ></Route>
        </Router>
    ), document.getElementById('app'));