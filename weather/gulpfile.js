'use strict';

var gulp = require('gulp');
var path = require('path');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var sequence = require('gulp-sequence');
var config = require('./config');

gulp.task('build', sequence('html', 'styles', 'fonts', 'scripts'));

gulp.task('html', function() {
    return gulp.src(path.join(config.source, config.tasks.html.input))
        .pipe(gulp.dest(config.dist));
});

gulp.task('styles', function() {
    return gulp.src(path.join(config.source, config.tasks.styles.folder, config.tasks.styles.input))
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.join(config.dist, config.tasks.styles.folder)))
});

gulp.task('fonts', function() {
    return gulp.src(path.join(config.source, config.tasks.fonts.folder, config.tasks.fonts.input))
        .pipe(gulp.dest(path.join(config.dist, config.tasks.fonts.folder)));
});

gulp.task('scripts', function() {
    return gulp.src(path.join(config.source, config.tasks.scripts.folder, config.tasks.scripts.input))
        .pipe(gulp.dest(path.join(config.dist, config.tasks.scripts.folder)))
});